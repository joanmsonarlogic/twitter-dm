 <?php
  session_start();
  require_once('includes/class.messages.php');
  require_once('includes/config_database.php');
  require_once('includes/config_token.php');
  require_once('includes/twitter_library.php');
  require_once('includes/twitter_utils.php');
  require_once('includes/dm_utils.php');
  $msg=new Messages();
?>

<html>
  
  <head>
    <title>post to twitter</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/mycss.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="icon"  href="images/tl.ico">
  </head> 
  
  <body>
    
    <div class="navbar-header pull-right links">
      <a href="addGroup.php" class="btn btn-default btn-sm">Group</a>
      <a href="setCredentials.php" class="btn btn-default btn-sm">Settings</a>
    </div>
    
    <!-- container -->
    <div class="container-fluid">
      
      <!-- header -->
      <div class="row" id="header">
        <img src="images/logo.png" id="logo" class="img img-responsive">
      </div>  
      <!-- end header -->
      
      <!-- first row -->
      <div class="row" >
        <br>
        
        <!-- column 1-->
        <div class="col-lg-offset-4 col-lg-4  col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8" id="holder">
          </br>
          <?php
            echo "<div style='width:80%; margin-left:10%;'>"; 
            echo $msg->display();
            echo "</div>"; 
          ?>
          </br>
          <h4> Post Tweet</h4>  
          
          <!-- status update form -->
          <form action="index.php" method="post">
            
            <textarea id="compose2" maxlength="140" class="form-control" placeholder="Compose new tweet . . ." name="status"></textarea>
            <br>
            <input type="submit" value="post" name="tweet" class="btn btn-primary">
          </form>
          
          <!-- end status update form -->
          <hr>
          
          <!-- nested row -->
          <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
              <h4>Send DM</h4>
            </div>
                
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
              <btn id="single_btn" class="btn btn-warning btn-sm">Single DM </btn> <btn id="group_btn" class="btn btn-success btn-sm">Group DM</btn>
            </div>
          
          </div>
          <!-- end nested row -->
          <br>
              
          <!-- Direct message form -->
          <form id="single_dm" action="index.php" method="post">
            
            <div class="row" >
              
              <div class="col-lg-2">
                <p> Followers</p>
              </div>
              <div class="col-lg-10">
                <select name="followers" class="form-control">
                  <option> Followers list</option>
                  <?php
                    foreach ($usersarray as $value) {
                      echo "<option>".$value["screen_name"]."</option>";
                    }
                  ?>
                </select >
              </div>
            </div>
                
            <br><br>
            <textarea name="msg" class="form-control" id="compose" maxlength="140" placeholder="Direct message . . ."></textarea>
            <br>
            <div class="row">
              
              <div class="col-lg-9">
              </div>
                  
              <div class="col-lg-3">
                <input type="submit" class="btn btn-primary" name="submit" value="send"/>
              </div>
            
            </div>
            
          </form>
          <!-- End form -->
          
          <!-- form2 -->
          <form id="group_dm" action="index.php" method="post">
              
            <div class="row" >
                
                <div class="col-lg-2">
                  <p>Groups</p>
                </div>
                
                <div class="col-lg-10">
                  <select name="followers2" class="form-control">
                    <option> Group list</option>
                    <option>All</option>
                    <?php
                      foreach ($group_array as $value) {
                        echo "<option>".$value["group_name"]."</option>";
                      }
                    ?>
                  </select >
                </div>
            
            </div>
            <br><br>
            <textarea name="msg2" class="form-control" id="compose3" maxlength="140" placeholder="Direct message . . ."></textarea>
            <br>
            
            <div class="row">
            
              <div class="col-lg-9">
                <p>Tags:</p>
                <input type="checkbox" name="append2" value="A"/>USERNAME<br>
                <input type="checkbox" name="append2" value="B"/>NAME<br>
              </div>
              
              <div class="col-lg-3">
                <input type="submit" class="btn btn-primary" name="submit2" value="send"/>
              </div>
            
            </div>
          
          </form>
          <!-- end form2 -->
        
        </div>
        <!--end column -->
      
      </div>  
      <!-- end first row -->
    
    </div>
    <!-- end container -->
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="bootstrap/js/myjs.js"></script> 
  
  </body>  

</html>