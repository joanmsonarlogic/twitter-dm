 <?php
  session_start();
   // ini_set('display_errors', 1);
  require_once('includes/class.messages.php');
  require_once('includes/twitter_library.php');
  require_once('includes/twitter_utils.php');
  require_once('includes/config_token.php');
  require_once('includes/config_database.php'); 
  $msg=new Messages();

  //fetch all twitter followers
  $return=fetch_followers($settings);
  if($return[0]==""){
    $msg->add($return[1],$return[2]);
  }
  else{
    $usersarray=$return[0];
  }
?>

<html>
  
  <head>
    <title>Create Group</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/mycss.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="icon"  href="images/tl.ico">
  </head>
  
  <body>
    
    <div class="navbar-header pull-right links">
      <a href="index.php" class="btn btn-default btn-sm">Home</a>
      <a href="setCredentials.php" class="btn btn-default btn-sm">Settings</a>
    </div>
    
    <!-- container -->
    <div class="container-fluid">
      
      <!-- header -->
      <div class="row" id="header">
         <img src="images/logo.png" id="logo" class="img img-responsive">
      </div>  
      <!-- end header -->
      
      <!-- first row -->
      <div class="row" >
        
        <br>
        <!-- column -->
        <div class="col-lg-offset-4 col-lg-4  col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8" id="holder">
          </br>
          <?php
            echo "<div style='width:80%; margin-left:10%;'>"; 
            echo $msg->display();
            echo "</div>"; 
          ?>
          </br>
          
          <div id="group_alert"> </div>
            
            </br>
            <h4> Create Group</h4>  
            <br>
            
            <!-- Direct message form -->
            <form action="" method="post">
              
              <div class="row">
                <div class="col-lg-4">
                  <p>Group name</p>
                </div>
              
                <div class="col-lg-8">
                  <input type="text" id="group_name" class="form-control" placeholder="Group name . . ."/>
                </div>
              </div>   
              <br>
                
              <div class="row">
                <div class="col-lg-4">
                      <p>Followers</p>
                </div>
                
                <div class="col-lg-8">
                  <select name="followers" id="list" class="form-control">
                    <option> Followers list</option>
                      <?php
                        if(isset($usersarray)){
                          foreach ($usersarray as $value) {
                            echo "<option class='users'>".$value["screen_name"]."</option>";
                          }
                        }
                      ?>
                  </select >
                </div>
              
              </div>
              </br><br>
              
              <div id="groupings">
                <p id="output" style="word-wrap:break-word;"> </p>
              </div>  
              <br>
              
              <btn id="group_submit" class="btn btn-success"> submit</btn>
              <br><hr><br>
              <h4>Update Group</h4>
              <br>
              
              <div class="row">
                <div class="col-lg-4">
                  <p>Groups</p>
                </div>
                    
                <div class="col-lg-8">
                  <select class="form-control" id="group_list">
                    <option>Group list</option>  
                    <?php
                      if(isset($dbh)){
                        $sql=$dbh->prepare("SELECT * FROM groups");
                        $sql->execute();
                        $group_array=$sql->fetchAll();
                        foreach ($group_array as $key) {
                         echo "<option>".$key["group_name"]."</option>";
                        }
                      }
                    ?>
                  </select>
                </div>
              
              </div>  
              <br><br>
              
              <div>
                <p id="output2" style="word-wrap:break-word;"> 
                </p>
              </div>
              <br>
              <input type="submit" id="update" class="btn btn-warning" value="update"></input>
              <input type="submit" id="delete" class="btn btn-danger" value="delete"></input>
            
            </form>
            <!-- End form -->
          
          </div>
          <!--end column -->
      
      </div>  
      <!-- end first row -->
    
    </div>
    <!-- end container -->
    
    <script src="bootstrap/js/jquery.js" type="text/javascript"></script>
    <script type="text/javascript" src="bootstrap/js/myjs.js"></script> 
  
  </body>  

</html>