<?php
  require_once('includes/config_database.php');
  require_once('includes/class.messages.php');

  //ajax fetch group
  if(isset($_POST['updata'])){
    
    $groupname = json_decode(stripslashes($_POST['updata']));
    $allmembers = json_decode(stripslashes($_POST['members']));

    //fetch group id of selected groupname
    $sql=$dbh->prepare("SELECT * FROM groups WHERE group_name ='$groupname'");
    $sql->execute();
    $group_id=$sql->fetchAll();
    
    foreach ($group_id as $key ) {
      $id=$key["id"];
    }

    //fetch group members of selected id
    $sql=$dbh->prepare("SELECT * FROM group_members WHERE group_id ='$id'");
    $sql->execute();
    $group_array=$sql->fetchAll();
  
    foreach ($allmembers as $index => $member) {
    
      foreach ($group_array as $g_array) {
        if($g_array["member_name"]==$member){
           unset($allmembers[$index]);
         }
      }
    }
    
    echo '<div class="row" class="followersrow">';
    echo '<div class="col-lg-4 followersrow">';
    echo '<p id="followerslabel">Followers</p>';
    echo '</div class="followersrow">';
    echo '<div class="col-lg-8 followersrow">';
    echo '<select name="followers" id="list2" class="form-control followersrow">';
    echo '<option class="followersrow"> Followers list</option>';
    
    foreach ($allmembers as $sub) {
      echo "<option class='users2 followersrow'>".$sub."</option>";
    }
    
    echo "</select class='users2 followersrow' >";
    echo "</div class='users2 followersrow'>";
    echo "<br class='users2 followersrow'>";
    echo "</div class='users2 followersrow'>";
    echo "<br class='followersrow'>";
    foreach ($group_array as $value) {
      echo "<span class='name2'>".$value["member_name"]."</span>";
    } 
  }
  //end fetch

  //ajax add group
  if (isset($_POST['data'])){
    $msg=new Messages();
    $data = json_decode(stripslashes($_POST['data']));
    $group = json_decode(stripslashes($_POST['groupname']));

    foreach($data as $d){
      $count++;
    }

    $dbh->exec("INSERT INTO groups(group_name,qty) VALUES ('$group','$count')");
    $lastid=$dbh->lastInsertId();  
  
    foreach($data as $d){
      $count++;
      $dbh->exec("INSERT INTO group_members(group_id,member_name) VALUES ('$lastid','$d')");
    }
    
    $msg->add('s',$group.' Group added');
    echo "<div style='width:80%; margin-left:10%;'>"; 
    echo $msg->display();
    echo "</div>"; 
  }
  //end add

  //ajax edit group
  if(isset($_POST['update'])){
    $data = json_decode(stripslashes($_POST['update']));
    $grpname=json_decode(stripslashes($_POST['grpname']));
    $count;

  //fetch group id of selected groupname
  $sql=$dbh->prepare("SELECT * FROM groups WHERE group_name ='$grpname'");
  $sql->execute();
  $group_id=$sql->fetchAll();
  foreach ($group_id as $key ) {
    $id=$key["id"];
  }
 
  //delete group members of selected id
  $sql2=$dbh->prepare("DELETE FROM group_members WHERE group_id ='$id'");
  $sql2->execute();

  foreach ($data as $key2 ) {
    $sql3=$dbh->prepare("INSERT INTO group_members(group_id,member_name) VALUES('$id','$key2')");
    $sql3->execute();
    $count++;
  }

  //update group members quantity
  $sql4=$dbh->prepare("UPDATE groups SET qty ='$count' WHERE id='$id'   ");
  $sql4->execute();

 
  $msg->add('s',$grpname.' Group updated');
      echo "<div style='width:80%; margin-left:10%;'>"; 
      echo $msg->display();
      echo "</div>"; 
  }
  //end edit

  //ajax delete group
  if(isset($_POST['grp_delete'])){
    $group = json_decode(stripslashes($_POST['grp_delete']));

    //fetch group id of selected groupname
    $sql=$dbh->prepare("SELECT * FROM groups WHERE group_name ='$group'");
    $sql->execute();
    $group_id=$sql->fetchAll();
  
    foreach ($group_id as $key ) {
      $id=$key["id"];
    }
  
    //delete group members of selected id
    $sql2=$dbh->prepare("DELETE FROM group_members WHERE group_id ='$id'");
    $sql2->execute();
  
    //delete group of selected id
    $sql2=$dbh->prepare("DELETE FROM groups WHERE id ='$id'");
    $sql2->execute();
  
    $msg->add('s',$group.' Group deleted');
    echo "<div style='width:80%; margin-left:10%;'>"; 
    echo $msg->display();
    echo "</div>"; 
  }
  //end delete

?>