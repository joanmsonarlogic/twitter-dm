<?php
	session_start();
  require_once("includes/save_token.php");
	require_once('includes/class.messages.php');
  $msg=new Messages();
?>

<html>
  
  <head>
    <title>Change Credentials</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/mycss.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="icon"  href="images/tl.ico">
  </head> 
  
  <body>
    
    <div class="navbar-header pull-right links">
      <a href="index.php" class="btn btn-default btn-sm">Home</a>
      <a href="addGroup.php" class="btn btn-default btn-sm">Group</a>
    </div>
    
    <!-- container -->
    <div class="container-fluid">
      
      <!-- header -->
      <div class="row" id="header">
        <img src="images/logo.png" id="logo" class="img img-responsive">
      </div>  
      <br>
      <!-- end header -->
      
      <!-- first column -->
      <div class="col-lg-offset-4 col-lg-4  col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8" id="holder">
          
          <!-- first nested row -->
          <div class="row">
            <?php
              echo "<div style='width:80%; margin-left:10%;'>"; 
              echo $msg->display();
              echo "</div>"; 
            ?>
          </div>
          <!-- end first nested row   -->

          <!-- 2nd nested row -->
          <div class="col-lg-12">
            </br>
			      <form action="setCredentials.php" method="post">
				      <input type="text" name="token" id="token" class="form-control" placeholder="Token" /><br />
				      <input type="text" name="tsecret" id="tsecret" class="form-control" placeholder="Token Secret"/><br />
				      <input type="text" name="consumer" id="consumer" class="form-control" placeholder="Consumer Key"/><br />
				      <input type="text" name="csecret" id="csecret" class="form-control" placeholder="Consumer Secret"/><br />
		 	        <input type="submit" name="submit" id="submit" value="Save" class="btn btn-warning form-control"/>
			       </form>
          </div>
          <!-- end 2nd nested row -->
      
      </div>
      <!-- end first column -->
    
    </div>
    <!-- end container -->
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="bootstrap/js/myjs.js"></script> 
  
  </body>  

</html>