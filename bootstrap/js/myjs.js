$(document).ready(function(){

	maxLength = $("#compose").attr("maxlength");
    $("#compose").after("<p style='color:blue; float:right;'><span id='remainingLengthTempId'>" 
        + maxLength + "</span> characters remaining</p>");
 	$("#compose").bind("keyup change", function(){checkMaxLength(this.id,maxLength); } )

    maxLength2 = $("#compose2").attr("maxlength");
    $("#compose2").after("<p style='color:blue;float:right;'><span id='remainingLengthTempId2'>" 
        + maxLength2 + "</span> characters remaining</p>");
 	$("#compose2").bind("keyup change", function(){checkMaxLength2(this.id,maxLength); } )

 	maxLength3 = $("#compose3").attr("maxlength");
    $("#compose3").after("<p style='color:blue;float:right;'><span id='remainingLengthTempId3'>" 
        + maxLength3 + "</span> characters remaining</p>");
 	$("#compose3").bind("keyup change", function(){checkMaxLength3(this.id,maxLength); } )
 
   $("#single_dm,#group_dm").hide();
   
   $("#single_btn").click(function(){
   		$("#single_dm").show();
   		$("#group_dm").hide();
   });

   $("#group_btn").click(function(){
   		$("#single_dm").hide();
   		$("#group_dm").show();
   });
 
	//fetch all members
	var fetchmem=[];
	$("#list>.users").each(function(){
		fetchmem.push($(this).text());
	});
	var allmem = JSON.stringify(fetchmem);
	//end fetch	



	//add members 
	$("#list").change(function(){
		
		//add span element
		var value=$(this).val();
		$("#output").append("<span class='name'>"+value+"</span>");
		//end add span element
		
		//remove selected option
		$("#list> option").each(function() {
   			if($(this).text()==value){
   				$(this).remove();
   			}
		});
		//end remove selected option

		//remove span and add option	
		$(".name").click(function(){
			var app= $(this).text();
			$("#list").append("<option class='user'>"+app+"</option>");					
			$(this).remove();

			//remove duplicate options
			var usedNames = {};
			$("#list > option").each(function(){
    			if(usedNames[this.text]) {
        			$(this).remove();
    			} 
    			else{
        			usedNames[this.text] = this.value;
    			}
			});
			//end remove duplicate options
						
		});
		//end  remove span and add option
	});
	//end add members


	//add new group	
	$("#group_submit").click(function(){
		var count=[];
		$("#output>span").each(function(){
			count.push($(this).text());
		});
		var group=$("#group_name").val();
		var ajaxData = JSON.stringify(count);
		var groupData = JSON.stringify(group);
		$.ajax({
  			url:"group_utils.php?",
  			type: 'post',
  			data:{groupname:groupData,data:ajaxData},
			success:function(dat) {
    			$("#group_alert").append(dat);
    			window.location.href = "addGroup.php?";
  			}
		});
	});
	//add submit new group

	//update group
	var upcount=[];
	$("#group_list").change(function(){
		delete group_update;
		group_update=$(this).val();
	});

	$("#update").click(function(){
			$("#output2>span").each(function(){
				upcount.push($(this).text());
			});
			var updatecount = JSON.stringify(upcount);
			var group_update_json = JSON.stringify(group_update);
			$.ajax({
  				url:"group_utils.php",
  				type: 'post',
  				data:{update:updatecount,grpname:group_update_json},
				success: function(dat) {
    				$("#group_alert").append(dat);
    				//window.location.href = "update_process.php";
  				}
			});
		});
	//end update


	//delete group	
	$("#group_list").change(function(){
		delete group_delete;
		group_delete=$(this).val();
	});

	$("#delete").click(function(){
			var group_delete_json = JSON.stringify(group_delete);
			$.ajax({
  				url:"group_utils.php?",
  				type: 'post',
  				data:{grp_delete:group_delete_json},
				success: function(dat) {
    				$("#group_alert").append(dat);
    				//window.location.href = "update_process.php";
  				}
			});
	});
	//end delete group


	//update group/append group members
	$("#group_list").change(function(){
		var update=$(this).val();
		var updateData = JSON.stringify(update);
		$.ajax({
		  	url:"group_utils.php?",
		 	type: 'post',
	 		data:{updata:updateData,members:allmem},
			success: function(dat) {
				$("#list2,#followerslabel,.followersrow").remove();
				$("#output2>span").each(function(){
					$(this).remove();
				});
				$("#output2").append(dat);

				$("#list2").change(function(){
					//add span element
					var value=$(this).val();
					$("#output2").append("<span class='name2'>"+value+"</span>");
					//end add span element

					//remove selected option
					$("#list2> option").each(function() {
   						if($(this).text()==value){
   							$(this).remove();
   						}
					});
					//end remove selected option

					//remove span and add option	
					$(".name2").click(function(){
						var app= $(this).text();
						$("#list2").append("<option class='users2'>"+app+"</option>");					
						$(this).remove();

						//remove duplicate options
						var usedNames = {};
						$("#list2 > option").each(function(){
    						if(usedNames[this.text]) {
        						$(this).remove();
    						} 
    						else{
        						usedNames[this.text] = this.value;
    						}
						});
						//end remove duplicate options
						
					});
					//end  remove span and add option



				});	

				//remove span and add option	
				$(".name2").click(function(){
					var app= $(this).text();
					$("#list2").append("<option class='users2'>"+app+"</option>");					
					$(this).remove();

					//remove duplicate options
					var usedNames = {};
					$("#list2 > option").each(function(){
    					if(usedNames[this.text]) {
        					$(this).remove();
    					} 
    					else{
        					usedNames[this.text] = this.value;
    					}
					});
					//end remove duplicate options
						
				});
				//end  remove span and add option

			}

		});
		//end ajax call
	});
//end append members

});


   function checkMaxLength(textareaID, maxLength){
 		currentLengthInTextarea = $("#"+textareaID).val().length;
        $(remainingLengthTempId).text(parseInt(maxLength) - parseInt(currentLengthInTextarea));
 		if (currentLengthInTextarea > (maxLength)) { 
 			// Trim the field current length over the maxlength.
			$("#compose").val($("#compose").val().slice(0, maxLength));
			$(remainingLengthTempId).text(0);
		}
    }

	function checkMaxLength2(textareaID, maxLength){
 		currentLengthInTextarea = $("#"+textareaID).val().length;
        $(remainingLengthTempId2).text(parseInt(maxLength) - parseInt(currentLengthInTextarea));
 		if (currentLengthInTextarea > (maxLength)) { 
 			// Trim the field current length over the maxlength.
			$("#compose2").val($("#compose2").val().slice(0, maxLength));
			$(remainingLengthTempId2).text(0);
		}
    }


    	function checkMaxLength3(textareaID, maxLength){
 		currentLengthInTextarea = $("#"+textareaID).val().length;
        $(remainingLengthTempId3).text(parseInt(maxLength) - parseInt(currentLengthInTextarea));
 		if (currentLengthInTextarea > (maxLength)) { 
 			// Trim the field current length over the maxlength.
			$("#compose3").val($("#compose3").val().slice(0, maxLength));
			$(remainingLengthTempId3).text(0);
		}
    }






