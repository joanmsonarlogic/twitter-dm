<?php
  
  if(isset($dbh)){
    //fetch all group members
    $sql=$dbh->prepare("SELECT * FROM groups");
    $sql->execute();
    $group_array=$sql->fetchAll();
    
    //fetch all twitter followers
    $return=fetch_followers($settings);
    if($return[0]==""){
      $msg->add($return[1],$return[2]);
    }
    else{
      $usersarray=$return[0];
    }

    //Status Update
    if(isset($_POST['tweet'])){
      $status=$_POST['status'];
      $status_return=status_update($settings,$status);
      $msg->add($status_return[0],$status_return[1]);
      unset($_POST['tweet']);
    } 

    //Single Dm  
    if(isset($_POST['submit'])){
      $message=$_POST['msg'];
      $followers=$_POST['followers'];
      $dm_return=send_dm($settings,$followers,$message,$dbh);
      $msg->add($dm_return[0],$dm_return[1]);
      unset($_POST['submit']);
    }   

    // Group Dm
    if(isset($_POST['submit2'])){
      $group_list=$_POST['followers2'];
      //if send to all friends   
      if($group_list=="All"){
        $message=$_POST['msg2'];
        $checked=$_POST['append2'];
        if($checked=="A"){
          $append_param="screen_name";
        }
        else{
          $append_param="name"; 
        }
        $all_return=dm_all($settings,$usersarray,$message,$append_param,$dbh);
        $msg->add($all_return[0],$all_return[1]);
      }
              
      // if send to group    
      else{ 
        $group=$_POST['followers2'];
        $message=$_POST['msg2'];
        $group_return=dm_group($settings,$group,$message,$dbh);
        $msg->add($group_return[0],$group_return[1]);
      } 
      unset($_POST['submit']);
    }
  }

?>