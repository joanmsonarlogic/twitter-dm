<?php
//Access tokens? see: https://dev.twitter.com/apps/

//get credentials on token_database
$filetxt ="includes/token_database.txt";
  if(file_exists($filetxt)) {
    $jsondata = file_get_contents($filetxt);
    $arr_data = json_decode($jsondata, true);
    $token=$arr_data["token"];
    $token_secret=$arr_data["tsecret"];
    $consumer=$arr_data["consumer"];
    $consumer_secret=$arr_data["csecret"];
  }
  else{
    echo 'The file '. $filetxt .' not exists';
  }


//config app credentials
  $settings = array(
    'oauth_access_token' => $token,
    'oauth_access_token_secret' => $token_secret,
    'consumer_key' => $consumer,
    'consumer_secret' => $consumer_secret
  );

?>