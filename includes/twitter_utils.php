<?php
	require_once('twitter_library.php');
	require_once('config_database.php');

//fetch all twitter followers
	function fetch_followers($settings){
		$url = 'https://api.twitter.com/1.1/followers/list.json';
		$getfield = '?username=J7mbo&skip_status=1';
		$requestMethod = 'GET';
		$twitter = new TwitterAPIExchange($settings);
		$jsondata = $twitter->setGetfield($getfield)
                      		->buildOauth($url, $requestMethod)
                      		->performRequest();  
  	$phparray= json_decode($jsondata,true); 
    //check for credential related errors  
   	if(isset($phparray)){
    	if (array_key_exists("users", $phparray)){
      	$usersarray=$phparray["users"];
    		$message_type="";
    		$message="";
    	}
    	else{
      	$errorcheck=$phparray["errors"][0]["message"];
      	if($errorcheck=="Invalid or expired token"){
        	$usersarray="";
        	$message_type ='e';
        	$message="<a href='setCredentials.php'>"."<p style='text-decoration:underline;'>Configure twitter app here</p>"."</a>";
      	}
      	else{
      		$usersarray="";
      		$message_type ='e';
        	$message=$errorcheck;
      	}
   		}
  	}
  	else{
  	  $usersarray="";
  		$message_type ='e';
    	$message="<a href='setCredentials.php'>"."<p style='text-decoration:underline;'>Configure twitter app here</p>"."</a>";
    }
		$fetch_followers=array($usersarray,$message_type,$message);
		return $fetch_followers;
	}

//Status update
  function status_update($settings,$status){
    $url = 'https://api.twitter.com/1.1/statuses/update.json';
    $requestMethod = 'POST';
    $postfields = array(
        'status' => $status 
        );
    $twitter = new TwitterAPIExchange($settings);
    $sentarray=$twitter->setPostfields($postfields)
                       ->buildOauth($url, $requestMethod)
                       ->performRequest();
         
    $phpsentarray= json_decode($sentarray,true);
         
    //Check if status is updated!
    if(isset($phpsentarray)){
      if (array_key_exists("id", $phpsentarray)){
          $message_type='s';
          $message='Status Updated';
      }
      else{
          $errormessage= $phpsentarray["errors"][0]["message"];
          $message_type='e';
          $message=$errormessage;
      }
      $status_array=array($message_type,$message);
      return $status_array;
    }
  }

// Send dm
  function send_dm($settings,$followers,$message,$dbh){
    $url = 'https://api.twitter.com/1.1/direct_messages/new.json';
    $requestMethod = 'POST';
    $postfields = array(
                'screen_name' => $followers, 
                'text' => $message
            );

    $twitter = new TwitterAPIExchange($settings);
    $sentarray=$twitter->setPostfields($postfields)
                       ->buildOauth($url, $requestMethod)
                       ->performRequest();
  
    $phpsentarray= json_decode($sentarray,true);
         
    //Check if the message is sent!
    if(isset($phpsentarray)){
      if (array_key_exists("id", $phpsentarray)){
        $message_type="s";
        $msg='message has been sent';
        $message_status="Succesful";
      }
      else{
        $errormessage= $phpsentarray["errors"][0]["message"];
        $message_type="e";
        $msg=$errormessage;
        $message_status="Failed";
      }
      if(isset($dbh)){
        $dbh->exec("INSERT INTO sent_messages(sending_type,qty) VALUES ('single',1)");
        $last_id = $dbh->lastInsertId();
        $recipient=$phpsentarray["recipient_screen_name"];
        $dbh->exec("INSERT INTO sent_details(sm_id,username,status,dates) VALUES ('$last_id','$recipient','$message_status',NOW())");
      }
      $dm_array=array($message_type,$msg);
      return $dm_array;
    }
  }

//Group dm
  function dm_group($settings,$group,$message,$dbh){
    if(isset($dbh)){
      //find group id
      $sql=$dbh->prepare("SELECT id FROM groups WHERE group_name='$group'");
      $sql->execute();
      $group_id=$sql->fetchAll();
        
      //find group members
      $groupid=$group_id[0]["id"];
      $sql2=$dbh->prepare("SELECT * FROM group_members WHERE group_id='$groupid'");
      $sql2->execute();
      $group_members_array=$sql2->fetchAll();
    }
     
    $array_id=array();  
    $count=0;
    $err=0;
      
    foreach ($group_members_array as $value) {
      $prepopulate="Hi ".$value['member_name']." ";
      $followers=$value["member_name"];
      $url = 'https://api.twitter.com/1.1/direct_messages/new.json';
      $requestMethod = 'POST';
      $postfields = array(
        'screen_name' => $followers, 
        'text' => $prepopulate.$message
      );
      
      $twitter = new TwitterAPIExchange($settings);
      $sentarray=$twitter->setPostfields($postfields)
                         ->buildOauth($url, $requestMethod)
                         ->performRequest();
        
      $phpsentarray= json_decode($sentarray,true);
          
        //Check if the message is sent!
        if(isset($phpsentarray)){
          if (array_key_exists("id", $phpsentarray)){
            $count++;
            $message_status="Succesful";
          }
          else{
            $err++;
            $message_status="Failed";
          }
          array_push($array_id,array($followers,$message_status));
        }
    }
    if($err==0){
      $message_status="i";
      $msg=$count.' total messages sent';
    }
    else{
      $message_status="i";
      $msg=$count.' messages sent and '.$err." failed sending";
    }  
    $group_array=array($message_status,$msg);
    $total_sent=$count+$err;
    
    if(isset($dbh)){
      $dbh->exec("INSERT INTO sent_messages(sending_type,qty) VALUES ('multiple','$total_sent')");
      $last_id = $dbh->lastInsertId();  
      
      foreach ($array_id as $key ){
        $recipient=$key[0];
        $message_status=$key[1];
        $dbh->exec("INSERT INTO sent_details(sm_id,username,status,dates) VALUES ('$last_id','$recipient','$message_status',NOW())");
      }
    }
    return $group_array;
  }  

//Dm all
  function dm_all($settings,$usersarray,$message,$append_param,$dbh){
    $count=0;
    $err=0;
    $array_id=array();
    foreach ($usersarray as $value) {
      $prepopulate="Hi ".$value[$append_param]." ";
      $followers=$value["screen_name"];
      $url = 'https://api.twitter.com/1.1/direct_messages/new.json';
      $requestMethod = 'POST';
      $postfields = array(
                      'screen_name' => $followers, 
                      'text' => $prepopulate.$message
                    );

      $twitter = new TwitterAPIExchange($settings);
      $sentarray=$twitter->setPostfields($postfields)
                        ->buildOauth($url, $requestMethod)
                        ->performRequest();
      $phpsentarray= json_decode($sentarray,true);
          
      //Check if the message is sent!
      if(isset($phpsentarray)){
        if (array_key_exists("id", $phpsentarray)){
          $count++;
          $message_status="Succesful";
        }
        else{
          $err++;
          $message_status="Failed";
        }
        array_push($array_id,array($followers,$message_status));
      }
    }

    if($err==0){
      $message_status="i";
       $msg=$count.' total messages sent';
    }
    else{
      $message_status="i";
      $msg=$count.' messages sent and '.$err." failed sending";
    }

    $all_array=array($message_status,$msg);
    $total_sent=$count+$err;
    if(isset($dbh)){
      $dbh->exec("INSERT INTO sent_messages(sending_type,qty) VALUES ('multiple','$total_sent')");
      $last_id = $dbh->lastInsertId();  
      foreach ($array_id as $key ) {
        $recipient=$key[0];
        $message_status=$key[1];
        $dbh->exec("INSERT INTO sent_details(sm_id,username,status,dates) VALUES ('$last_id','$recipient','$message_status',NOW())");
      }
    }
    return $all_array;
  } 
?>