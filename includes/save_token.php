<?php
require_once('class.messages.php');
$msg=new Messages();

if(isset($_POST['token']) && isset($_POST['tsecret']) && isset($_POST['consumer']) && isset($_POST['csecret'])) {
  
  if(empty($_POST['token']) || empty($_POST['tsecret']) || empty($_POST['consumer']) || empty($_POST['csecret'])) {
    $msg->add('e','All fields are required');
  }
  else {
    // adds form data into an array
    $formdata = array(
      'token'=> $_POST['token'],
      'tsecret'=> $_POST['tsecret'],
      'consumer'=> $_POST['consumer'],
      'csecret'=> $_POST['csecret']
    );
    //end

    // encodes the array into a string in JSON format (JSON_PRETTY_PRINT - uses whitespace in json-string, for human readable)
    $jsondata = json_encode($formdata, JSON_PRETTY_PRINT);

    if(file_put_contents('includes/token_database.txt', $jsondata)) $msg->add('s','Data successfully saved'."<br>"."<a href='index.php'><p style='text-decoration:underline;'>Go back to main page</p></a>");
    else $msg->add('e','Unable to save data in token_database.txt');
  }
}
else $msg->add('w',"<a href='https://dev.twitter.com/apps/'>"."<p style='text-decoration:underline;'>Get your access tokens here</p>"."</a>");

